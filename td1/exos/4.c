#include <string.h>
#include <stdio.h>
#include <stdlib.h>

void concat(char *str1, char *str2,char *buf){
  snprintf(buf,sizeof(buf),"%s%s",str1,str2);
}

int main(){ 
  char *buf = malloc(sizeof(char)*(strlen("hello") + strlen("world")));
  concat("hello", "world", buf); 
  printf("RESULT : %s\n", buf); 
  free(buf);
  return 0; 
}
