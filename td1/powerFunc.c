#include <stdio.h>

int power(int a, int n, int* ints)
{
    *ints+=2;
    if( n != 0 )
        return a*power(a, n - 1, ints);
    else
        return 1;
}

void main (int argc, char* argv)
{
    int ints = 0;
    char str[15];
    int result = power(2, 3, &ints);
    printf("%i\n", ints*sizeof(int));
}
