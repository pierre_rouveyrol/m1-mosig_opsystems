#include "mem_alloc.h"
#include <stdio.h>
#include <assert.h>
#include <string.h>

/* memory */
char memory[MEMORY_SIZE]; 

/* Structure declaration for a free block */
typedef struct free_block{
	int size; 
	struct free_block *next; 
} free_block_s, *free_block_t; 

/* Structure declaration for an occupied block */
typedef struct{
	int size; 
} busy_block_s, *busy_block_t; 

/* Pointer to the first free block in the memory */
free_block_t first_free; 

#define ULONG(x)((long unsigned int)(x))
#define max(x,y) (x>y?x:y)

void memory_init(void)
{
	first_free = (free_block_t)memory;
	first_free->size = MEMORY_SIZE;
	first_free->next = NULL;
}

void find_first_fit(free_block_t *previous, free_block_t *current, int required_size)
{
	while(required_size > (*current)->size && (*current)->next != NULL)
	{
        if(((char*)((*current)->next) - memory) > MEMORY_SIZE || (*current)->size >MEMORY_SIZE) //Corruption check
        {
            printf("Error : Arrrr! Memory corruption happened mate! Integrity compromised, I'm outta here!\n");
            exit(1);
        }

		*previous=*current;
       	*current=(*current)->next;
	}
}

void find_best_fit(free_block_t *previous, free_block_t *current, int required_size)
{
    find_first_fit(previous, current, required_size); //failsafe : find the first good block
    free_block_t previous_temp = *previous;
    free_block_t current_temp = *current;
    while(current_temp->next != NULL)
    {
		previous_temp=current_temp;
       	current_temp=current_temp->next;
        if(current_temp->size > required_size && current_temp->size < (*current)->size)
        {
            *previous=previous_temp;
            *current=current_temp;
        }
    }
}

void find_worst_fit(free_block_t *previous, free_block_t *current, int required_size)
{
    free_block_t previous_temp = *previous;
    free_block_t current_temp = *current;
    while(current_temp->next != NULL)
    {
		previous_temp=current_temp;
       	current_temp=current_temp->next;
        if(current_temp->size > required_size && current_temp->size > (*current)->size)
        {
            *previous=previous_temp;
            *current=current_temp;
        }
    }
}

char *memory_alloc(int size)
{
	free_block_t previous = first_free;
	free_block_t current = first_free;
	char* addr = NULL;
	int required_size = 0; 
	int actual_size = 0;

	if(size + sizeof(busy_block_s) < sizeof(free_block_s))
		required_size = sizeof(free_block_s);
	else
		required_size = sizeof(busy_block_s) + size;

//	find_first_fit(&previous, &current, required_size);
	find_best_fit(&previous, &current, required_size);
//	find_worst_fit(&previous, &current, required_size);

	if(current != NULL)
	{
		free_block_t next = NULL;
		if(current->size - required_size < sizeof(free_block_s))
		{
			next = current->next;
			actual_size = current->size - sizeof(busy_block_s);
		}
		else
		{
			next = (free_block_t)((char*)current+required_size);
			next->size = current->size - required_size;
			next->next = current->next;
			actual_size = required_size - sizeof(busy_block_s);
		}

		if(previous != current)
			previous->next = next;
		else
		{ 
			first_free=next;
		}
		busy_block_t newBusy = (busy_block_t)current; 
		addr = (char*)newBusy + sizeof(busy_block_s); 
		newBusy->size = required_size;
	}
	print_alloc_info(addr, actual_size);
	return addr;
}

void merge_contiguous(void)
{
	free_block_t current = first_free;
	free_block_t previous = first_free;
    int merged = 0;

	while(current->next != NULL)
	{
		previous=current;
		current=current->next;
		
        if((char*)current == (((char*)previous) + previous->size))
		{
			previous->size += current->size;
			previous->next = current->next;
            merged = 1;
		}
	}
    if(merged == 1)
        merge_contiguous();
}

void memory_free(char *p){
	print_free_info(p); 

	busy_block_t toFree  = (busy_block_t)((char*)p - sizeof(busy_block_s));

    if(toFree->size == 0 || toFree->size > MEMORY_SIZE) //test if the pointer is correct
    {
        printf("Error : trying to free a non-allocated pointer!\n");
        exit(1);
    }
    
    free_block_t newFree = (free_block_t)toFree;
	newFree->size = toFree->size;

	free_block_t current = first_free;
	free_block_t previous = first_free;

	while((char*)current < (char*)newFree)
	{
		previous=current;
		current=current->next;
	}

 	if((char*)previous > (char*)newFree) //chunk before first_free
 	{
 		newFree->next = first_free;
 		first_free = newFree;
 	}

	else //chunck surrounded by free blocks of after the last
	{
		newFree->next = previous->next;
		previous->next = newFree;
	}
	merge_contiguous();
}

void print_info(void) {
	fprintf(stderr, "Memory : [%lu %lu] (%lu bytes)\n", (long unsigned int) 0, (long unsigned int) (memory+MEMORY_SIZE), (long unsigned int) (MEMORY_SIZE));
	fprintf(stderr, "Free block : %lu bytes; busy block : %lu bytes.\n", ULONG(sizeof(free_block_s)), ULONG(sizeof(busy_block_s))); 
}

void print_free_info(char *addr){
	if(addr)
		fprintf(stderr, "FREE  at : %lu \n", ULONG(addr - memory)); 
	else
		fprintf(stderr, "FREE  at : %lu \n", ULONG(0)); 
}

void print_alloc_info(char *addr, int size){
	if(addr){
		fprintf(stderr, "ALLOC at : %lu (%d byte(s))\n", 
			ULONG(addr - memory), size);
	}
	else{
		fprintf(stderr, "Warning, system is out of memory\n"); 
	}
}

void print_free_blocks(void) {
	free_block_t current; 
	fprintf(stderr, "Begin of free block list :\n"); 
	for(current = first_free; current != NULL; current = current->next)
		fprintf(stderr, "Free block at address %lu, size %u\n", ULONG((char*)current - memory), current->size);
}

char *heap_base(void) {
	return memory;
}


void *malloc(size_t size){
	static int init_flag = 0; 
	if(!init_flag){
		init_flag = 1; 
		memory_init(); 
    //print_info(); 
	}      
	return (void*)memory_alloc((size_t)size); 
}

void free(void *p){
	if (p == NULL) return;
	memory_free((char*)p); 
	print_free_blocks();
}

void *realloc(void *ptr, size_t size){
	if(ptr == NULL)
		return memory_alloc(size); 
	busy_block_t bb = ((busy_block_t)ptr) - 1; 
	printf("Reallocating %d bytes to %d\n", bb->size - (int)sizeof(busy_block_s), (int)size); 
	if(size <= bb->size - sizeof(busy_block_s))
		return ptr; 

	char *new = memory_alloc(size); 
	memcpy(new, (void*)(bb+1), bb->size - sizeof(busy_block_s) ); 
	memory_free((char*)(bb+1)); 
	return (void*)(new); 
}

#ifdef MAIN
int main(int argc, char **argv){

  /* The main can be changed, it is *not* involved in tests */
	memory_init();
	print_info(); 
	print_free_blocks();
	int i ; 
	for( i = 0; i < 10; i++){
		char *b = memory_alloc(rand()%8);
		memory_free(b); 
		print_free_blocks();
	}

	char * a = memory_alloc(15);
	a=realloc(a, 20); 
	memory_free(a);


	a = memory_alloc(10);
	memory_free(a);

	printf("%lu\n",(long unsigned int) (memory_alloc(9)));
	return EXIT_SUCCESS;
}
#endif 
