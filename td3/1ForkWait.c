#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>


int main(int argc, char **argv){
    int n;
    if(argc > 1)
        n = atoi(argv[1]);
    else
    {
        printf("Please provide an int as parameter\n");
        exit(0);
    }
    
    pid_t pid = getpid();

    for(int i = 0; i<n  ; i++)
    {
        pid = fork();
        if(pid == 0)
        {
            sleep(1);
            printf("%d\n",getpid());
            exit(0);
        }
    }

    for(int i = 0; i<n ; i++)
    {
        wait(NULL);
    }
        
    exit(0);
}
