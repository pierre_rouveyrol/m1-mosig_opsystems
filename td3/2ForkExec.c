#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>


int main(int argc, char **argv){
    pid_t pid = getpid();
    pid = fork();

    if(pid == 0)
    {
        execl("/bin/ls", "ls", NULL);
        exit(0);
    }
    else
    {
        printf("Hello World!\n");
        wait(NULL);
        exit(0);
    }
}
