#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>


int main(int argc, char **argv){
    pid_t pid = getpid();
    int p[2];
    pipe(p);
    
    pid = fork();

    if(pid == 0) //child
    {
        close(p[1]);
        read(p[0], &pid, sizeof(pid_t));
        printf("Child says : %d\n",pid);
        exit(0);
    }
    else //father
    {
        close(p[0]);
        write(p[1], &pid, sizeof(pid_t));
        printf("Father says : %d\n",pid);
        wait(NULL);
        exit(0);
    }
}
