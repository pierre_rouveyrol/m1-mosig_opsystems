#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>

int timeSlept;

void handler(int signal) 
{ 
    printf("Process %d slept for %d before receiving signal %d\n", getpid(),timeSlept, signal); 
    exit(0); 
}

int main(int argc, char **argv){
    int n;
    if(argc > 1)
        n = atoi(argv[1]);
    else
    {
        printf("Please provide an int as parameter\n");
        exit(0);
    }
    
    signal(SIGINT, handler);

    for(int i=0; i<n; i++)
    {
        timeSlept = i;
        sleep(1);
    }

    exit(0);
}
