#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>

void handler(int signal) 
{ 
    printf("SIGALRM : %d received\n", signal); 
    return;
}

int main(int argc, char **argv){

    signal(SIGALRM, handler);
    
    alarm(1);
    pause();
    alarm(6);
    pause();

    exit(0);
}
