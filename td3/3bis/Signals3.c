#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

int nbChld;

void handler(int signal) 
{ 
    int child_status; 
    for(int i=0; i<nbChld;i++)
    {
        waitpid(-1, &child_status, WNOHANG);
    } 
} 


int main(int argc, char **argv){
    if(argc > 1)
        nbChld = atoi(argv[1]);
    else
    {
        printf("Please provide an int as parameter\n");
        exit(0);
    }

    pid_t pid;
    signal(SIGCHLD, handler);
   
    for(int i=0; i<nbChld; i++)
    {
        pid = fork();

        if(pid != 0) //father
            wait(NULL);
        else
        {
            printf("Hello! I'm %d!\n",getpid());
            fflush(stdout);
            exit(0);
        }
    }

    exit(0);
}
