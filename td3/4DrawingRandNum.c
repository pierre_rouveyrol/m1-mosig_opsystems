#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>


int main(int argc, char **argv){
    int n;
    if(argc > 1)
        n = atoi(argv[1]);
    else
    {
        printf("Please provide an int as parameter\n");
        exit(0);
    }
    
    pid_t pid = getpid();
    int p[2];

    for(int i = 0; i<n ; i++)
    {
        pipe(p);
        pid = fork();
        
        if(pid!=0) //father
        {
            close(p[0]);
            write(p[1], &i, sizeof(int));
        }
        else //children
        {
            close(p[1]);
            int order;
            read(p[0], &order, sizeof(int));
            srand(order+10);
            int randNum = rand();

            printf("processus pid %d node %d val = %d \n",getpid(),order, randNum);
            exit(0);
        }
    }

    for(int i = 0; i<n ; i++)
    {
        wait(NULL);
    }
        
    exit(0);
}
