#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>


int main(int argc, char **argv){
    int n;
    if(argc > 1)
        n = atoi(argv[1]);
    else
    {
        printf("Please provide an int as parameter\n");
        exit(0);
    }
    
    pid_t pid = getpid();
    int p[2];
    int ring[n][2]; //array of pipes

    typedef struct //struct used to pass winner info
    {
        int pid;
        int order;
        int value;
    } data;

    for(int i = 0; i<n ; i++)
        pipe(ring[i]); //creating all the pipes

    for(int i = 0; i<n ; i++)
    {
        pipe(p);
        pid = fork();
        
        if(pid!=0) //father
        {
            close(p[0]);
            write(p[1], &i, sizeof(int));

            for(int i = 0; i<n ; i++) //closing all the pipes not used by the father.
            {
                close(ring[i][0]);
                close(ring[i][1]);
            }
        }
        else //children
        {
            close(p[1]);
            int order;
            read(p[0], &order, sizeof(int));
            srand(order+10);
            int randNum = rand();

            printf("processus pid %d node %d val = %d \n",getpid(),order, randNum);
            
            data Dat;

            if(order == 0)//first child initializes the struct before passing it to the next child.
            {
                Dat.pid = getpid();
                Dat.order = order;  
                Dat.value = randNum;
                printf("Written : %d \n",Dat.value);

                close(ring[order][0]);
                write(ring[order][1], &Dat, sizeof(data));
                close(ring[order][1]);
                exit(0);
            }
            else
            {
                close(ring[order-1][1]);
                read(ring[order-1][0], &Dat, sizeof(data));
                printf("Read : %d\n", Dat.value);

                if(Dat.value < randNum) //If this child is the new winner
                {
                    Dat.pid = getpid();
                    Dat.order = order;
                    Dat.value = randNum;
                }
                
                if(order != n-1) //if not the last one, pass the winner along
                {
                    close(ring[order][0]);
                    write(ring[order][1],&Dat,sizeof(data));
                    exit(0);
                }
                else //print the winner
                {
                    printf("winner is processus pid %d node %d val = %d \n",Dat.pid, Dat.order, Dat.value);
                }
            }
            exit(0);
        }
        //wait(NULL); //waiting here to have the children go in order.
    }
    for(int i = 0; i<n ; i++)
        wait(NULL);
    exit(0);
}