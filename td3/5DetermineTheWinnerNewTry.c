#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>


int main(int argc, char **argv){
    int n;
    if(argc > 1)
        n = atoi(argv[1]);
    else
    {
        printf("Please provide an int as parameter\n");
        exit(0);
    }
    
    pid_t pid = getpid();
    int p[2];
    int q[2]; 

    typedef struct //struct used to pass winner info
    {
        int pid;
        int order;
        int value;
    } data;
    
    data Dat;

    for(int i = 0; i<n ; i++)
    {
        pipe(p);
        pipe(q);
        pid = fork();
        
        if(pid!=0) //father
        {
            close(p[0]);
            write(p[1], &i, sizeof(int));
            if(i!=0)
                write(p[1], &Dat, sizeof(data));

        }
        else //children
        {
            close(p[1]);
            int order;
            read(p[0], &order, sizeof(int));
            srand(order+10);
            int randNum = rand();

            printf("processus pid %d node %d val = %d \n",getpid(),order, randNum);

            if(order == 0)//first child initializes the struct before passing it to the next child.
            {
                Dat.pid = getpid();
                Dat.order = order;  
                Dat.value = randNum;
            }
            else
            {
                read(p[0], &Dat, sizeof(data));
                printf("Read : %d\n", Dat.value);

                if(Dat.value < randNum) //If this child is the new winner
                {
                    Dat.pid = getpid();
                    Dat.order = order;
                    Dat.value = randNum;
                }
               
            }
            close(q[0]);
            printf("Written : %d \n",Dat.value);
            write(q[1],&Dat,sizeof(data));
            exit(0);
        }
        wait(NULL); //waiting here to have the children go in order.
    }

    printf("winner is processus pid %d node %d val = %d \n",Dat.pid, Dat.order, Dat.value);
    exit(0);
}
