#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>
#define PERMS (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)


void * create_shared_array (int segment_size)
{
    key_t key ;
    int shmid ;
    void *ptrS ;

    key = ftok ("./7SharedMemory", 'a') ;
    
    if (key == -1)
    {
        fprintf (stderr, "ftok error\n") ;
        exit (-1) ;
    }
    shmid = shmget (key, segment_size, IPC_CREAT|PERMS) ;
    
    if (shmid == -1)
    {
        fprintf (stderr, "shmget error\n") ;
        exit (-1) ;
    }
    
    ptrS = shmat (shmid, NULL, 0) ;
    
    if (ptrS == (void *) -1)
    {
        fprintf (stderr, "shmat error\n") ;
        exit (-1) ;
    }
    return ptrS ;
}


void detach_shared_array (void *ptrS)
{
    int cr ;
    cr = shmdt (ptrS);
    if (cr == -1)
    {
        fprintf (stderr, "shmdt error\n") ;
        exit (-1) ;
    }
}


int main(int argc, char **argv){
    int n;
    if(argc > 1)
        n = atoi(argv[1]);
    else
    {
        printf("Please provide an int as parameter\n");
        exit(0);
    }
    
    pid_t pid = getpid();
    int p[2];
    int* ptrS = create_shared_array(3*sizeof(int));

    for(int i = 0; i<n ; i++)
    {
        pipe(p);        
        pid = fork();
        
        if(pid!=0) //father
        {
            close(p[0]);
            write(p[1], &i, sizeof(int));

            
        }
        else //children
        {
            close(p[1]);
            int order;
            read(p[0], &order, sizeof(int));
            srand(order+10);
            int randNum = rand();

            printf("processus pid %d node %d val = %d \n",getpid(),order, randNum);
            
            if(order == 0)//first child initializes the struct before passing it to the next child.
            {
                ptrS[0] = getpid();
                ptrS[1] = order;
                ptrS[2] = randNum;

                exit(0);
            }
            else
            {
                if(ptrS[2] < randNum) //If this child is the new winner
                {
                    ptrS[0] = getpid();
                    ptrS[1] = order;
                    ptrS[2] = randNum;
                }
                
                if(order == n-1) //print the winner
                {
                    printf("winner is processus pid %d node %d val = %d \n",ptrS[0], ptrS[1], ptrS[2]);
                }
            }
            exit(0);
        }
        wait(NULL); //waiting here to have the children go in order.
    }
    detach_shared_array(ptrS);
    exit(0);
}
