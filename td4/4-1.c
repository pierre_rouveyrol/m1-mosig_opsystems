#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

//Generates a tab of size N with numbers between 0 and 49,999,999
void initRandVect(int* tab, int n)
{
    srand(time(0));
    for(int i=0; i<n; i++)
    {
        tab[i]=rand()%50000000;
    }
}

void search(int * T, int n, int x)
{
    clock_t cstart = clock();
    clock_t cend = 0;
    for(int i=0; i<n; i++)
    {
        if(T[i] == x)
        {
            cend = clock();
            printf("first appearance at index : %d\n",i);
            printf ("%d cpu clocks\n", ((int)cend - (int)cstart));
            exit(0);
        
    }
    cend = clock();
    printf("%d\n",-1);
    printf ("%d cpu clocks\n", ((int)cend - (int)cstart));
    exit(0);
}

void main(int argc, char** argv)
{
   /* if(argc != 3)
    {
        printf("call this way : 4-1 tabsize target");
        exit(EXIT_FAILURE);
    }

    int n = atoi(argv[1]);
    int target = atoi(argv[2]);*/
    int n = 100000000;
    int target = 25000000;
    static int tab[100000000];
    
    initRandVect(tab, n);

    search(tab, n, target);
}
