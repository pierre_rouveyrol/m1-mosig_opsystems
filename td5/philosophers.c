#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#define NB_PHILO 5

pthread_t th[NB_PHILO];
pthread_cond_t condition;
pthread_mutex_t mutex;
int chopsticks[NB_PHILO];
int NbFeeds[NB_PHILO];

void handler(int signal)
{
    for(int i=0; i<NB_PHILO; i++)
        printf("Philosopher %d ate %d times\n",i,NbFeeds[i]);
    exit(0);
}

void acquire_chopsticks(int n){ //section critique
    pthread_mutex_lock(&mutex);
    while(!(chopsticks[n]==-1 && chopsticks[(n+1)%NB_PHILO]==-1)){ //the CS are not available
        pthread_cond_wait(&condition, &mutex);
        printf("Philosopher %d failed to acquire the chopsticks, waiting\n", n);
    }
    printf("Philosopher %d aquired the chopsticks, starts eating\n", n);
    chopsticks[n] = n;
    chopsticks[(n+1)%NB_PHILO] = n;
    pthread_mutex_unlock(&mutex);
}

void release_chopsticks(int n){
    pthread_mutex_lock(&mutex);
    chopsticks[n] = -1;
    chopsticks[(n+1)%NB_PHILO] = -1;
    pthread_cond_signal(&condition);
    pthread_mutex_unlock(&mutex);
}

void eat(int n){
    printf("Philosopher %d tries to acquire chopsticks\n", n);
    acquire_chopsticks(n);
    NbFeeds[n]++;
	sleep((rand()%5));
    release_chopsticks(n);
    printf("Philosopher %d is done eating, back to thinking\n", n);
}

void think(int n){
    printf("Philosopher %d starts thinking\n", n);
    sleep((rand()%5)); //The philosopher is thinking
}

void* bePhilosophical(void* arg){
	int n = (int)arg;
	think(n);
	while(1)
	{
		eat(n);
		think(n);
	}
}

void main(int argc, char** argv){
    pthread_cond_init(&condition, NULL);
    pthread_mutex_init(&mutex, NULL);
    
    signal(SIGINT, handler);

	for(int i=0; i<NB_PHILO; i++){ //initialize chopsticks
        chopsticks[i]=(-1);
    }

	for(int i=0; i<NB_PHILO; i++){
        NbFeeds[i]=0;
    }

	for(int i=0; i<NB_PHILO; i++){
        pthread_create(&th[i], NULL, bePhilosophical, i);
    }

	for(int i=0; i<NB_PHILO; i++){
        pthread_join(th[i], NULL);
    }
}
