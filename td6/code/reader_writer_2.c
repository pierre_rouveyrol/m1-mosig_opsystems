#include "reader_writer.h"
#include "reader_writer_tracing.h"


extern tracing_t t; 

typedef struct reader_writer{
  pthread_mutex_t lock;
  pthread_cond_t cWrite;
  int readCount;
} reader_writer_s; 

reader_writer_t rw_init(){
  reader_writer_t rw = malloc(sizeof(reader_writer_s)); 

  pthread_mutex_init(&rw->lock, NULL);
  rw->readCount = 0;

  return rw; 
}

void begin_read(reader_writer_t rw){
    pthread_mutex_lock(&rw->lock);
    rw->readCount++;
    tracing_record_event(t, BR_EVENT_ID);
    pthread_mutex_unlock(&rw->lock);
}

void end_read(reader_writer_t rw){
    pthread_mutex_lock(&rw->lock);
    rw->readCount--;
    tracing_record_event(t, ER_EVENT_ID);  
    
    if(rw->readCount == 0) //If we are the last reader, signal the waiting writer that he can go.
        pthread_cond_signal(&rw->cWrite);
    
    pthread_mutex_unlock(&rw->lock);
}

void begin_write(reader_writer_t rw){ //The writer will wait until able to write to take the lock.
    pthread_mutex_lock(&rw->lock);
    while(rw->readCount > 0)
        pthread_cond_wait(&rw->cWrite, &rw->lock);
    tracing_record_event(t, BW_EVENT_ID);  
}

void end_write(reader_writer_t rw){
    tracing_record_event(t, EW_EVENT_ID);
    pthread_mutex_unlock(&rw->lock);
}

